#' Load magnetometre data from csv file
#' 
#' \code{load_magneto} loads the csv from the magnetometre into R
#' 
#' @param filename magnetometre acquisition
#' @return An R matrix that contains magnetometre data
#' @examples
#' \dontrun{
#' load_magneto("data-raw/rotation_alea.csv")
#' }
#' @export
load_magneto <- function(filename) {
  magneto <- as.matrix(readr::read_csv(filename, col_names = F))
  if(dim(magneto)[2] == 1) {
    magneto <- as.matrix(readr::read_csv2(filename, col_names = F))
  }
  magneto
}

#' Compute raw positions
#'
#' \code{compute_A} computes the raw positions of the pearl in a 3D space 
#' based on the acquisition of a rotation with a magnetometre considering 
#' the magnetometre's 25 sensors.
#'
#' @param magneto R matrix from a magnetometre acquisition
#' 
#' @examples
#' \dontrun{
#' compute_A(axial_rotation)
#' }
#' @export
compute_A <- function(magneto) {
  magneto <- correction(magneto)
  coordinates <- magneto %*% transfert_mat
  coordinates
  #TODO
  #allow to use an other model of sensors (like 10)
  #magneto_to_coo_10(filename)
}

#' Compute smoothed positions
#'
#' \code{compute_B} smooths the raw positions from compute_A. 
#' compute_B intends to filters the noice of the acqusition.
#'
#' @param A raw positions from compute_A
#' @param step.size size of the smoothing window
#' 
#' @examples
#' \dontrun{
#' A <- compute_A(axial_rotation)
#' compute_B(A)
#' }
#' @export
compute_B <- function(A, step.size = 20) {
  B <- matrix(0, nrow = dim(A)[1] - 1 - step.size, ncol = 1)
  coeff24hMA = matrix(1,nrow = 1, ncol = step.size)/step.size
  B <- stats::filter(A, coeff24hMA, sides = 1)
  B <- B[step.size:(nrow(B) - step.size),]
  B
}

#' Find the best fitting sphere for a set of 3D points.
#'
#' \code{compute_best_sphere} fit a sphere.
#' TODO : Use the original doc
#'
#' @param B smoothed positions from compute_A
#' 
#' @examples
#' \dontrun{
#' A <- compute_A(axial_rotation)
#' B <- compute_B(A)
#' compute_best_sphere(B)
#' }
#' @export
compute_best_sphere <- function(B) {
  sphere <- sphere_fit(B)
  if (sphere$radius < 1) {
    sphere$center <- c(mean(B[,1]), mean(B[,2]), mean(B[,3]))
  }
  sphere
}

#' Project a set of points on a sphere
#'
#' \code{compute_C} projects smoothed positions from compute_B on the best 
#' fitting sphere.
#'
#' @param B smoothed positions from compute_B
#' @param sphere best fitting sphere to B
#' 
#' @examples
#' \dontrun{
#' A <- compute_A(axial_rotation)
#' B <- compute_B(A)
#' sphere <- compute_best_sphere(B)
#' compute_C(B, sphere)
#' }
#' @export
compute_C <- function(B, sphere) {
  C <- apply(B, 1, project_on_sphere, 
             center = sphere$center, 
             radius = sphere$radius)
  t(C)
}

project_on_sphere <- function(point, center, radius) {
  u <- point - center
  r <- radius/sqrt(sum(u^2))
  center + r*u
}

#' Projects points describing a rotating shpere to the intersection 
#' of the spheroid with the plane perpendicular to its axis of rotation.
#' 
#' \code{compute_D} uses positions from compute_C and modify it to 
#' allow to compute relevant metrics of the rotation
#'
#' @param C positions at the sufrace of a sphere from compute_C
#' @param sphere corresponding sphere
#' @param WMax size of the window taken into account for the axis of rotation
#' 
#' @examples
#' \dontrun{
#' A <- compute_A(axial_rotation)
#' B <- compute_B(A)
#' sphere <- compute_best_sphere(B)
#' C <- compute_C(B, sphere)
#' compute_D(C, sphere)
#' }
#' @export
compute_D <- function(C, sphere, WMax = 40) {
  N <- dim(C)[1]
  D <- 0*C
  for (k in 1:N) {
    # Ensemble de points considérés pour l'axe de rotation
    if (k >= WMax & k <=N-WMax) {
      x = C[(k - (WMax-1)):(k + WMax),1]
      y = C[(k - (WMax-1)):(k + WMax),2]
      z = C[(k - (WMax-1)):(k + WMax),3]  
    } else if (k < WMax) {
      x = C[1:(2*WMax - 1),1]
      y = C[1:(2*WMax - 1),2]
      z = C[1:(2*WMax - 1),3]  
    } else {
      x = C[(N - (2*WMax - 1)):(N),1]
      y = C[(N - (2*WMax - 1)):(N),2]
      z = C[(N - (2*WMax - 1)):(N),3]  
    }
    
    # Plan qui fit au mieux x, y, z
    Coefficients <- stats::lsfit(cbind(x, y), z)$coefficients
    XCoeff <- Coefficients[2]
    YCoeff <- Coefficients[3]
    CCoeff <- Coefficients[1]
    
    # Plan parallèle passant par le centre de la sphère
    CCoeff2 = sphere$center[3] - XCoeff*sphere$center[1] - YCoeff*sphere$center[2]
    
    a2 = XCoeff
    b2 = YCoeff
    c2 = -1
    d2 = CCoeff2
    
    # Plan perpendiculaire passant par le centre de la sphère et le point de C
    xA=C[k,1];
    yA=C[k,2];
    zA=C[k,3];        
    xB=sphere$center[1];
    yB=sphere$center[2];
    zB=sphere$center[3];
    xC=xA+XCoeff;
    yC=yA+YCoeff;
    zC=zA-1;
    
    a = (yB - yA)*(zC - zA) - (zB - zA)*(yC - yA) ;
    b = - ( (xB - xA)*(zC - zA) - (zB -zA)*(xC - xA) ) ;
    c = (xB - xA)*(yC - yA) - (yB - yA)*(xC - xA) ;
    d = - (a*xA + b*yA + c*zA) ;
    
    # Vecteur directeur de l'intersection de (a, b, c, d) et (a2, b2, c2, d2)
    u.coeff = (a*b2 - a2*b)*(a*c2 - a2*c)/(a*d2 - a2-d)
    u.x = (b*c2 - b2*c)/u.coeff
    u.y = -(a*c2 - a2*c)/u.coeff
    u.z = (a*b2 - a2*b)/u.coeff
    u = c(u.x, u.y, u.z)
    
    # 
    r <- sphere$radius/norm(u, type = "2")
    I1 <- sphere$center + r*u
    I2 <- sphere$center - r*u
    
    D1 <- sqrt(sum((C[k,] - I1)^2)) # distance(B[i,], I1)
    D2 <- sqrt(sum((C[k,] - I2)^2)) # distance(B[i,], I2)
    
    if (D1 < D2) {
      D[k,] <- I1
    } else {
      D[k,] <- I2
    }
  }
  D
}

#' Modelise the rotation of the pearl
#'
#' \code{modelise_rotation} computes all the steps, A, B, C and D to modelise
#' the rotation of a pearl from the acqusition with a magnetometre
#'
#' @param filename magnetometre acquisition
#' @return A list containing all sets of points A, B, C and D 
#'   and the corresponding sphere
#' @examples
#' \dontrun{
#' compute_rotation("data-raw/rotation_axiale.csv")
#' }
#' @export
modelise_rotation <- function(filename) {
  magneto <- load_magneto(filename)
  A       <- compute_A(magneto)
  B       <- compute_B(A)
  sphere  <- compute_best_sphere(B)
  C       <- compute_C(B, sphere)
  D       <- compute_D(C, sphere)
  list(magneto = magneto, A = A, B = B, sphere = sphere, C = C, D = D)
}