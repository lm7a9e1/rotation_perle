transfert_mat <- as.matrix(readr::read_csv("data-raw/transfert_mat.csv", col_names = F) %>%
                             dplyr::rowwise() %>%
                             dplyr::mutate_all(.funs = function (x) eval(parse(text = x))))

usethis::use_data(transfert_mat, internal = TRUE)
