# Measuring pearl rotation

Cultured pearls are produced by first making a grafting operation, during which a small piece of mantle tissue from a donor oyster (the graft) is inserted in the gonad of the recipient oyster together with a nacre bead, the nucleus. The magnetometer is device that monitors the nucleus rotating during its formation in the gonad of the pearl oyster.

The magnetometer is made up of three main connected parts. The measuring part is a dome with sensors consisting of a half-sphere of acrylic glass (diameter 20 cm) on which are set 25 magnetic sensors each consisting of two components, an HMC1021 compass from Honeywell (a one-axis magnetic sensor) and an offset compensation circuit. The movement of a pearl is recorded using a magnetometer in pearl oysters grafted with magnetized nuclei.

The output of the magnetometer is a CSV file with 25 columns corresponding to the 25 magnetic sensors and line for each acquisition. The purpose of this package is to:

* traduce this data into a rotation movement
* display the rotation path
* characterize the rotation with descriptive metrics

# Usage

## Install from gitlab

```{r, eval=FALSE}
remotes::install_gitlab("lm7a9e1/rotation_perle", host ="gitlab.ifremer.fr")
library(pearlrotation)
```

## Load a magnetometer acquisition

The CSV file should directly come from a magnetometer acquisition and can be directly loaded into R :

```{r, eval=FALSE}
magneto <- load_magneto("magneto.csv")
```

## Traduce the acquisition into a rotation movement

It takes four steps to interpret the magnetometer acquisition:

* **A**: compute raw pistions of the pearl in a 3D space
* **B**: smooths the raw positions from step **A**
* **C**: adjust smoothed positions from step **B** on the best fitting sphere 
* **D**: adapt the positions follow a movement perpendicular to the axis of rotation

```{r, eval=FALSE}
A <- compute_A(axial_rotation)
B <- compute_B(A)
sphere <- compute_best_sphere(B)
C <- compute_C(B, sphere)
D <- compute_D(C, sphere)
```

## Display the rotation path

Once the step **D** reached it is possible to diplay a 3D model of the pearl with the rotation path in a rgl frame :

```{r, eval=FALSE}
plot_pearl(D, sphere)
#TODO:plot_and_save_pearl
animate_pearl(D, sphere)
animate_and_save_pearl(D, sphere, "plots/axial_rotation")
```

## Characterize the rotation with descriptive metrics

### Rotation speed

The magnetometer acquire data with a constant time step wich allows to compute the rotation speed using from the angle between each acquisition:

```{r, eval=FALSE}
compute_angles(D, sphere$center)
#TODO:compute_speed(D, sphere$center, acqusition.period)
#TODO:compute_mean_speed(D, sphere$center, acquisition.period)
#TODO:compute_min_speed(D, sphere$center, acquisition.period)
#TODO:compute_..._speed(D, sphere$center, acquisition.period)
```

## Get a diagram of the magnetometer's sensors

```{r, eval=FALSE}
plot_magnetometre_diagram()
```
